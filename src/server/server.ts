import { ApolloServer } from 'apollo-server'
import { default as exportsQueries } from './../plugins/exports/queries'
import { default as exportsResolvers } from './../plugins/exports/resolvers'

const typeDefs = [
  exportsQueries
]

// A map of functions which return data for the schema.
const resolvers = [
  exportsResolvers
]

const server = new ApolloServer({
  typeDefs,
  resolvers
})

server.listen().then(({ url }) => {
  console.log(`Server listening on ${url}`)
})
