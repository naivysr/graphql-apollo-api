import * as Bunyan from 'bunyan'
import * as moment from 'moment'

class Logger {
  private reqResLogger: any
  private serverLogger: any
  private path: string

  constructor() {
    this.path = './logs/'
    this.createLoggers()
  }

  createLogger(name: string, level: string): void {
    const options = {
      name: 'GraphQL Debugger',
      streams: [
        {
          path: `${this.path}${name}.log`,
          level
        }
      ],
      time: moment().format()
    }

    return Bunyan.createLogger(options)
  }

  createLoggers(): void {
    this.reqResLogger = this.createLogger('req-res-details', 'trace')
    this.serverLogger = this.createLogger('server', 'fatal')
  }

  request(request: any, resData: any): void {
    try {
      const { body } = request
      const key = Object.keys(body)[0]
      const temp = body[key].replace(/\r?\n|\r/g, '')

      body[key] = temp

      const info = {
        method: request.method,
        request: {
          headers: request.headers,
          body,
          params: request.params,
          query: request.query
        },
        response: {
          statusCode: request.res.statusCode,
          result: JSON.parse(resData)
        }
      }

      this.reqResLogger.trace(info)
    } catch (error) {
      // do not log
      return
    }
  }

  server(error: any): void {
    const info = {
      error
    }

    this.serverLogger.fatal('Details: ', info)
  }
}

export default new Logger()
