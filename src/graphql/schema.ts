import {
  GraphQLSchema, GraphQLObjectType, GraphQLString,
  GraphQLList, GraphQLBoolean, GraphQLNonNull, GraphQLInt
} from 'graphql'

import { ApolloServer, ApolloError, ValidationError, gql } from 'apollo-server'

// queries - GET requests
const Query = new GraphQLObjectType({
  name: 'Query',
  description: '',
  fields: () => ({
    // queries here
  })
})

export default new GraphQLSchema({
  query: Query
})
