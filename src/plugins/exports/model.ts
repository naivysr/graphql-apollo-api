import * as QueryBuilder from 'bodybuilder'

import ElasticSearch from './../ElasticSearch'

class ExportsModel {
  constructor() { }

  async getDocuments(ids: string[]): Promise<any> {
    try {
      const query = QueryBuilder()
        .query('terms', '_id', ids)
        .build()

      const data = await ElasticSearch.query(query, 0, 100)
      return data.hits.hits.map(hit => hit._source)
    } catch (error) {
      return Promise.reject(error)
    }
  }
}

export default ExportsModel
