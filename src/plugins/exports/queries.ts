import { gql } from 'apollo-server'

export default gql`
  type Query {
    document(ids: [String]!): [documentFields]
  }

  type documentFields {
    "Query to get fields from a document"
    sku: String
    sku_description: String
    skukey: String
    sku_type: String
    name: String
    name_expanded: String
    name_abbreviated: String
    name_web: String
    name_web_breakdown: [NameBreakdown]
    brand: String
    sub_brand: String
    is_in_store: String
    hts_code: String
    lp_code: String
    cost_average: String
    cost_unit: String
    cost_bridge: String
    cost_warehouse: String
    cost_freight_charge: String
    custom_attributes: [String]
    generation: String
    overview: String
    overview_html: String
    details: String
    details_html: String
    features: String
    features_html: String
    faq: String
    faq_html: String
    assembly: String
    assembly_html: String
    minimum_order_qty: String
    stain_protection_available: Boolean
    inhome_service_available: Boolean
    is_active: Int
    is_delivery_chargeable: Int
    delivery_charge: String
    is_on_sale: Int
    sale_start_date: String
    sale_end_date: String
    is_clearance: Int
    is_in_clearance_center: Boolean
    clearance_rating: String
    clearance_rating_description: String
    is_online: Int
    omit_from_exception: Int
    case_package: String
    cubic_feet: String
    cubic_feet_boxed: String
    safety_stock_days: String
    unit_of_measure: String
    price_type: String
    price_retail: String
    price_sale: String
    price_group: String
    price_sale_group: String
    price_online: String
    price_online_sale: String
    price_package: String
    price_national: String
    price_homestore: String
    location_store: Int
    location_availability: Int
    auto_ordered: Int
    fabric_protection_charge: String
    assembly_charge: String
    size: String
    weight: String
    length_inches: String
    width_inches: String
    height_inches: String
    length_inches_assembled: String
    width_inches_assembled: String
    height_inches_assembled: String
    diameter_inches_assembled: String
    retail_amount_highest: String
    selling_group: String
    buying_group: String
    buyer: String
    vendor_number: String
    vendor_code: String
    vendor_initials: String
    vendor_name: String
    mattress_comfort_level: String
    factory: String
    class_id: String
    subclass_id: String
    subgroup_id: String
    class_name: String
    subclass_name: String
    subgroup_name: String
    color: String
    color_code: String
    material: String
    collection: String
    collection_code: String
    collections_by_rooms: [CollectionsByRoom]
    shape: String
    style: String
    styles: [Style]
    gender: String
    availability: String
    skuset: [Skuset]
    suggested_skus: [AssociatedSku]
    related_skus: [AssociatedSku]
    stores: [Store]
    rooms: [Room]
    categories: [Category]
    subcategories: [Subcategory]
    divisions: [Division]
    price_zone: [String]
    price_zone_parent: String
    clearance_images_exist: Int
    images_exist: Int
    images: [Image]
    el_date: String
    po_existing: Boolean
  }

  type NameBreakdown {
    html_encoding: String
    web_modifier1: String
    web_modifier2: String
  }

  type CollectionsByRoom {
    collection_code: String
    collection_short_description: String
    collection_long_description: String
  }

  type Style {
    style_key: String,
    style_label: String,
    style_label_raw: String,
  }

  type Skuset {
    child_id: String
    qty: String
    required: Boolean
    type: String
  }

  type AssociatedSku {
    sku: String
    rank: Int
  }

  type Store {
    store_id: String
  }

  type Room {
    child_id: String
    qty: String
    required: Boolean
    type: String
  }

  type Category {
    parentId: Int
    id: Int
    name: String
    name_raw: String
    visible: Boolean
  }

  type Subcategory {
    parentId: String
    id: Int
    name: String
    name_raw: String
    visible: Boolean
  }

  type Division {
    id: String
  }

  type Image {
    index: Int
    type: String
    angle: String
    sequence: Int
    url: String
  }  `
