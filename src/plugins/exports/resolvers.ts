import { default as ExportsModel } from './model'

export default {
  Query: {
    document: async (parent, args, context, info) => {
      try {
        const exportsObj = new ExportsModel()
        const data = await exportsObj.getDocuments(args.ids)
        return data
      } catch (error) {
        return []
      }
    }
  }
}
