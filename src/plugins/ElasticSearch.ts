import * as AWS from 'aws-sdk'
import * as Elastic from 'elasticsearch'
import { promisify } from 'util'
import { get } from 'config'

class ElasticSearch {
  private config: any

  constructor() {
    this.config = get('application.elastic')
  }

  client(): any {
    // AWS.config.update({ region: this.config.aws.region })
    // AWS.config.update({
    //   credentials: new AWS.Credentials(process.env.AWS_ES_ACCESS_KEY,
    //     process.env.AWS_ES_ACCESS_SECRET)
    // })

    const dev = !this.config.host.includes('aws')
    const connection = dev ? {} : { connectionClass: require('http-aws-es') }

    const client = new Elastic.Client({
      host: `${this.config.host}`,
      // protocol: this.config.protocol,
      log: this.config.log,
      // timeout: this.config.timeout,
      requestTimeout: this.config.requestTimeOut,
      // ...connection
    })

    return client
  }

  ping(): Promise<any> {
    return new Promise((resolve, reject) => {
      const client = this.client()
      const params = {
        method: 'GET'
      }
      client.ping(params, (error, res, status) => {
        client.close()
        if (error) {
          return reject('Can not ping Elastic Search')
        } else {
          return {
            status: true,
            data: 'PONG'
          }
        }
      })
    })
  }

  query(queryObject: any, from: number, size: number): Promise<any> {
    return new Promise((resolve, reject) => {
      const client = this.client()

      const requestObject: any = {
        index: this.config.index.alias,
        from,
        size,
        body: queryObject
      }

      client.search(requestObject, (error, response) => {
        if (error) {
          return reject(`Cannot query Elastic Search ${error.message}`)
        }

        client.close()
        return resolve(response)
      })
    })
  }

}

export default new ElasticSearch()
